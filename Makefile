CARGO := cargo

DEBUG := 0
ifeq ($(DEBUG), 0)
	CARGO_OPTIONS := --release --locked
else
	CARGO_OPTIONS :=
endif

.PHONY: all gitlab-exporter lint clean

all: gitlab-exporter lint

gitlab-exporter:
	$(CARGO) build $(CARGO_OPTIONS)

lint:
	$(CARGO) fmt -- --check
	$(CARGO) check
	$(CARGO) clippy --all -- -D warnings -A clippy::derive_partial_eq_without_eq
	$(CARGO) deny check

clean:
	$(CARGO) clean
