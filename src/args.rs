use std::path::PathBuf;

use clap::{ArgAction, Parser, Subcommand, ValueHint};
/// Extracts metrics from GitLab's GraphQL API and produce prometheus consumable metrics
#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// GitLab GraphQL API URL.
    #[arg(
        short = 'u',
        long,
        env = "GITLAB_EXPORTER_GITLAB_API_URL",
        default_value = "https://gitlab.archlinux.org/api/graphql"
    )]
    pub api_url: String,
    /// Number of top-of metrics to calculate
    #[arg(
        short = 'n',
        long = "top_nth",
        env = "GITLAB_EXPORTER_TOP_NTH",
        default_value = "10"
    )]
    pub top_nth: usize,
    /// Show progress indicator
    #[arg(short = 'p', long = "progress_bar")]
    pub progress_bar: bool,
    /// Verbose mode (-v, -vv, -vvv, etc.)
    #[arg(
    short,
    long,
    action = ArgAction::Count,
    alias = "debug"
    )]
    pub verbose: u8,
    /// Output file, stdout if not present
    #[arg(
    short,
    long,
    value_parser = clap::value_parser!(PathBuf),
    value_hint = ValueHint::AnyPath
    )]
    pub output: Option<PathBuf>,
    #[command(subcommand)]
    pub command: Command,
}

#[derive(Debug, Subcommand)]
pub enum Command {
    /// Execute once and output in cli
    Cli,
}

#[test]
#[cfg(test)]
fn verify_args() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
