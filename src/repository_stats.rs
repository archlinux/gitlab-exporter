#[derive(Debug, Clone)]
pub struct RepositoryStatistics {
    repository_name: String,
    repository_size: i64,
    build_artifacts_size: i64,
    commit_count: i64,
    storage_size: i64,
    lfs_objects_size: i64,
    packages_size: i64,
    pipeline_artifacts_size: i64,
    snippets_size: i64,
    uploads_size: i64,
    wiki_size: i64,
}

impl RepositoryStatistics {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        repository_name: String,
        repository_size: i64,
        build_artifacts_size: i64,
        commit_count: i64,
        storage_size: i64,
        lfs_objects_size: i64,
        packages_size: i64,
        pipeline_artifacts_size: i64,
        snippets_size: i64,
        uploads_size: i64,
        wiki_size: i64,
    ) -> RepositoryStatistics {
        RepositoryStatistics {
            repository_name,
            repository_size,
            build_artifacts_size,
            commit_count,
            storage_size,
            lfs_objects_size,
            packages_size,
            pipeline_artifacts_size,
            snippets_size,
            uploads_size,
            wiki_size,
        }
    }

    pub fn repository_name(&self) -> &str {
        &self.repository_name
    }
    pub fn repository_size(&self) -> i64 {
        self.repository_size
    }
    pub fn build_artifacts_size(&self) -> i64 {
        self.build_artifacts_size
    }
    pub fn commit_count(&self) -> i64 {
        self.commit_count
    }
    pub fn storage_size(&self) -> i64 {
        self.storage_size
    }
    pub fn lfs_objects_size(&self) -> i64 {
        self.lfs_objects_size
    }
    pub fn packages_size(&self) -> i64 {
        self.packages_size
    }
    pub fn pipeline_artifacts_size(&self) -> i64 {
        self.pipeline_artifacts_size
    }
    pub fn snippets_size(&self) -> i64 {
        self.snippets_size
    }
    pub fn uploads_size(&self) -> i64 {
        self.uploads_size
    }
    pub fn wiki_size(&self) -> i64 {
        self.wiki_size
    }
}
