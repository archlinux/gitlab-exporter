use crate::error::{Error, Result};
use crate::graphql::{GraphQL, Metrics};
use graphql_client::{GraphQLQuery, Response};
use log::warn;
use std::convert::TryInto;

use backon::ExponentialBuilder;
use backon::Retryable;

type Time = String;

macro_rules! gen_admin_query {
    ($name: ident,
    $var: ident,
    metric: $metric: expr) => {
        #[derive(GraphQLQuery)]
        #[graphql(
            schema_path = "graphql/schema.json",
            query_path = "graphql/queries.graphql",
            response_derives = "Serialize,PartialEq, Debug",
            normalization = "rust"
        )]
        pub struct $name;
        impl $name {
            pub async fn run(graphql: &GraphQL) -> Result<Metrics> {
                let mut metrics = Metrics::new();
                if let Some(utm) = Self::query(graphql, $var::Variables {})
                    .await?
                    .data
                    .ok_or(Error::MissingData("response data"))?
                    .usage_trends_measurements
                {
                    let mut count: i64 = 0;
                    if let Some(e) = utm.edges {
                        count += e
                            .iter()
                            .flatten()
                            .map(|trends| trends.node.as_ref())
                            .flatten()
                            .next()
                            .ok_or(Error::MissingData("trends node"))?
                            .count
                    }
                    metrics.add_metric(String::from($metric), count.try_into()?)
                }
                Ok(metrics)
            }
            async fn query(
                graphql: &GraphQL,
                variables: $var::Variables,
            ) -> Result<Response<$var::ResponseData>> {
                let request_body = Self::build_query(variables);

                let closure = || async {
                    let response: Response<$var::ResponseData> = graphql
                        .client()
                        .post(graphql.gitlab_url())
                        .json(&request_body)
                        .send()
                        .await?
                        .json()
                        .await?;
                    if let Some(errors) = response.errors {
                        let messages: Vec<_> = errors.iter().map(|e| format!("{:}", e)).collect();
                        warn!("GraphQL error (retry): {:?}", messages);
                        return Err(Error::FailedGraphQL(messages));
                    }
                    Ok(response)
                };
                let response = closure.retry(&ExponentialBuilder::default()).await?;
                Ok(response)
            }
        }
    };
}

gen_admin_query!(UsersCount, users_count, metric: "trends_total_users");
gen_admin_query!(ProjectsCount, projects_count, metric: "trends_total_projects");
gen_admin_query!(GroupsCount, groups_count, metric: "trends_total_groups");
