mod admin_queries;
pub mod args;
mod error;
mod graphql;
mod queries;
mod repository_stats;

use crate::admin_queries::{GroupsCount, ProjectsCount, UsersCount};
use crate::args::{Args, Command};
use crate::graphql::GraphQL;
use crate::queries::RepositorySize;
use anyhow::{Context, Result};
use futures::future::BoxFuture;
use indicatif::{HumanDuration, ProgressBar, ProgressStyle};
use lazy_static::lazy_static;
use log::debug;
use prometheus::{opts, Encoder, IntGauge, IntGaugeVec, Opts, Registry, TextEncoder};
use std::convert::TryInto;
use std::time::{Duration, Instant};
use std::{env, fs};

lazy_static! {
    static ref TOTAL_BUILD_TRENDS_TOTAL_USER_GAUGE: IntGauge =
        IntGauge::new("gitlab_exporter_trends_total_users", "trends total users")
            .expect("metric can be created");
    static ref TOTAL_BUILD_ARTIFACTS_SIZE_GAUGE: IntGauge = IntGauge::new(
        "gitlab_exporter_total_build_artifacts_size",
        "total build artifacts size"
    )
    .expect("metric can be created");
    static ref REGISTRY: Registry = Registry::new();
}

pub async fn run(args: Args) -> Result<()> {
    match args.command {
        Command::Cli => {
            let started = Instant::now();
            let pb: Option<ProgressBar> = initialize_progress_bar(&args);

            let api_token = &env::var("GITLAB_EXPORTER_GITLAB_TOKEN")
                .context("Missing env var GITLAB_EXPORTER_GITLAB_TOKEN")?;

            let graphql = GraphQL::new(args.api_url, api_token)?;
            let handles: Vec<BoxFuture<_>> = vec![
                Box::pin(UsersCount::run(&graphql)),
                Box::pin(ProjectsCount::run(&graphql)),
                Box::pin(GroupsCount::run(&graphql)),
                Box::pin(RepositorySize::run(&graphql)),
            ];
            let results = futures::future::join_all(handles).await.into_iter();
            let gauge_vec_repository_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_repository_sizes",
                    "gitlab exporter top repository sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_repository_sizes.clone()))?;

            let gauge_vec_build_artifacts_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_build_artifacts_sizes",
                    "gitlab exporter top build artifacts sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_build_artifacts_sizes.clone()))?;

            let gauge_vec_commit_count = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_commit_counts",
                    "gitlab exporter top commit counts"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_commit_count.clone()))?;

            let gauge_vec_storage_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_storage_sizes",
                    "gitlab exporter top storage sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_storage_sizes.clone()))?;

            let gauge_vec_lfs_objects_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_lfs_objects_sizes",
                    "gitlab exporter top lfs objects sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_lfs_objects_sizes.clone()))?;

            let gauge_vec_packages_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_packages_sizes",
                    "gitlab exporter top package sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_packages_sizes.clone()))?;

            let gauge_vec_pipeline_artifacts_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_pipeline_artifacts_sizes",
                    "gitlab exporter top pipeline artifacts sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_pipeline_artifacts_sizes.clone()))?;

            let gauge_vec_snippets_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_snippets_sizes",
                    "gitlab exporter top snippets sizes sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_snippets_sizes.clone()))?;

            let gauge_vec_uploads_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_uploads_sizes",
                    "gitlab exporter top uploads sizes sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_uploads_sizes.clone()))?;

            let gauge_vec_wiki_sizes = IntGaugeVec::new(
                opts!(
                    "gitlab_exporter_top_wiki_sizes",
                    "gitlab exporter top wiki sizes sizes (in bytes)"
                ),
                &["fullname"],
            )?;
            REGISTRY.register(Box::new(gauge_vec_wiki_sizes.clone()))?;
            for metric in results {
                let metric = metric?;
                for (key, value) in metric.get_metrics() {
                    let mut help_text = key.to_string().replace('_', " ");
                    if help_text.contains("size") {
                        help_text.push_str(" (in bytes)");
                    }
                    let gauge = IntGauge::with_opts(Opts::new(key, help_text))?;
                    REGISTRY.register(Box::new(gauge.clone()))?;
                    gauge.set(usize::try_into(value)?);

                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.repository_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.repository_size())
                            })
                            .iter()
                        {
                            gauge_vec_repository_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.build_artifacts_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.build_artifacts_size())
                            })
                            .iter()
                        {
                            gauge_vec_build_artifacts_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.commit_count())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.commit_count())
                            })
                            .iter()
                        {
                            gauge_vec_commit_count.with_label_values(&[name]).set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.storage_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.storage_size())
                            })
                            .iter()
                        {
                            gauge_vec_storage_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.lfs_objects_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.lfs_objects_size())
                            })
                            .iter()
                        {
                            gauge_vec_lfs_objects_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.packages_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.packages_size())
                            })
                            .iter()
                        {
                            gauge_vec_packages_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.pipeline_artifacts_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.pipeline_artifacts_size())
                            })
                            .iter()
                        {
                            gauge_vec_pipeline_artifacts_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.snippets_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.snippets_size())
                            })
                            .iter()
                        {
                            gauge_vec_snippets_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.uploads_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.uploads_size())
                            })
                            .iter()
                        {
                            gauge_vec_uploads_sizes
                                .with_label_values(&[name])
                                .set(*size);
                        }
                    }
                    if !metric
                        .get_top_nth_filter_by(args.top_nth, &|r| {
                            (r.repository_name().to_string(), r.wiki_size())
                        })
                        .is_empty()
                    {
                        for (name, size) in metric
                            .get_top_nth_filter_by(args.top_nth, &|r| {
                                (r.repository_name().to_string(), r.wiki_size())
                            })
                            .iter()
                        {
                            gauge_vec_wiki_sizes.with_label_values(&[name]).set(*size);
                        }
                    }
                }
            }
            if let Some(pb) = pb {
                pb.finish_with_message(format!("Done in {}", HumanDuration(started.elapsed())));
            }
            let mut buffer = vec![];
            let encoder = TextEncoder::new();
            let metric_families = REGISTRY.gather();
            encoder.encode(&metric_families, &mut buffer)?;
            match args.output {
                Some(output) => fs::write(output, buffer)?,
                None => println!("{}", String::from_utf8(buffer)?),
            }
        }
    }
    Ok(())
}

fn initialize_progress_bar(args: &Args) -> Option<ProgressBar> {
    if args.progress_bar {
        debug!("Initializing progress indicator");
        let pb = ProgressBar::new_spinner();
        pb.enable_steady_tick(Duration::from_millis(200));
        pb.set_style(
            ProgressStyle::with_template("{spinner:.blue} {msg}")
                .unwrap()
                .tick_strings(&[".  ", ".. ", "...", " ..", "  .", "   "]),
        );
        pb.set_message("Extracting...");
        Some(pb)
    } else {
        None
    }
}
