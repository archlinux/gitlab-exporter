use crate::error::{Error, Result};
use crate::graphql::{GraphQL, Metrics};
use crate::repository_stats::RepositoryStatistics;
use backon::ExponentialBuilder;
use backon::Retryable;
use graphql_client::{GraphQLQuery, Response};
use log::warn;
use rayon::prelude::IntoParallelRefIterator;
use rayon::prelude::*;
use std::convert::TryInto;
use std::sync::Mutex;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/schema.json",
    query_path = "graphql/queries.graphql",
    response_derives = "Serialize,PartialEq, Debug",
    variable_derives = "Serialize,PartialEq, Debug",
    normalization = "rust"
)]
pub struct RepositorySize;

impl RepositorySize {
    pub async fn run(graphql: &GraphQL) -> Result<Metrics> {
        let mut end_cursor = String::new();

        let response = Self::query(graphql, end_cursor.to_string()).await?;
        let mut projects = response
            .data
            .ok_or(Error::MissingData("response data"))?
            .projects
            .ok_or(Error::MissingData("projects"))?;
        let mut edges = Vec::new();
        if let Some(mut e) = projects.edges {
            edges.append(&mut e);
        }
        let mut more_data = projects.page_info.has_next_page;
        while more_data {
            end_cursor = projects
                .page_info
                .end_cursor
                .as_ref()
                .map(String::from)
                .ok_or(Error::MissingData("end cursor"))?;

            let response = Self::query(graphql, end_cursor.to_string()).await?;
            projects = response
                .data
                .ok_or(Error::MissingData("response data"))?
                .projects
                .ok_or(Error::MissingData("projects"))?;

            if let Some(mut e) = projects.edges {
                edges.append(&mut e);
            }
            more_data = projects.page_info.has_next_page;
        }
        let repo_stats_temp = Mutex::new(Vec::new());
        edges
            .par_iter()
            .flatten()
            .map(|edge| edge.node.as_ref().ok_or(Error::InsufficientPermission))
            .for_each(|node| {
                if let Ok(e) = node {
                    let repository_full_name = &e.full_path;
                    if let Some(stats) = &e.statistics.as_ref() {
                        let repo_stats = RepositoryStatistics::new(
                            String::from(repository_full_name),
                            stats.repository_size as i64,
                            stats.build_artifacts_size as i64,
                            stats.commit_count as i64,
                            stats.storage_size as i64,
                            stats.lfs_objects_size as i64,
                            stats.packages_size as i64,
                            stats.pipeline_artifacts_size.unwrap_or(0.0) as i64,
                            stats.snippets_size.unwrap_or(0.0) as i64,
                            stats.uploads_size.unwrap_or(0.0) as i64,
                            stats.wiki_size.unwrap_or(0.0) as i64,
                        );
                        repo_stats_temp
                            .lock()
                            .expect("Expected stat, but found nothing")
                            .push(repo_stats);
                    }
                }
            });
        let repo_stats_vec = repo_stats_temp
            .lock()
            .expect("Expected stat, but found nothing")
            .to_vec();
        let repository_size = repo_stats_vec
            .iter()
            .map(|r| r.repository_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let build_artifacts_size = repo_stats_vec
            .iter()
            .map(|r| r.build_artifacts_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let commit_count = repo_stats_vec
            .iter()
            .map(|r| r.commit_count())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let storage_size = repo_stats_vec
            .iter()
            .map(|r| r.storage_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let lfs_objects_size = repo_stats_vec
            .iter()
            .map(|r| r.lfs_objects_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let packages_size = repo_stats_vec
            .iter()
            .map(|r| r.packages_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let pipeline_artifacts_size = repo_stats_vec
            .iter()
            .map(|r| r.pipeline_artifacts_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let snippets_size = repo_stats_vec
            .iter()
            .map(|r| r.snippets_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let uploads_size = repo_stats_vec
            .iter()
            .map(|r| r.uploads_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));
        let wiki_size = repo_stats_vec
            .iter()
            .map(|r| r.wiki_size())
            .fold(0 as f64, |sum, x| sum + (x as f64));

        let mut metrics = Metrics::new();
        metrics.add_metric(
            String::from("total_build_artifacts_size"),
            (build_artifacts_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_commit_count"),
            (commit_count as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_lfs_objects_size"),
            (lfs_objects_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_packages_size"),
            (packages_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_pipeline_artifacts_size"),
            (pipeline_artifacts_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_repository_size"),
            (repository_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_snippets_size"),
            (snippets_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_storage_size"),
            (storage_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_uploads_size"),
            (uploads_size as i64).try_into()?,
        );
        metrics.add_metric(
            String::from("total_wiki_size"),
            (wiki_size as i64).try_into()?,
        );
        metrics.set_repository_statistics_vector(repo_stats_vec);
        Ok(metrics)
    }

    async fn query(
        graphql: &GraphQL,
        cursor: String,
    ) -> Result<Response<repository_size::ResponseData>> {
        let closure = || async {
            let response = graphql
                .client()
                .post(graphql.gitlab_url())
                .json(&RepositorySize::build_query(repository_size::Variables {
                    page_size: graphql.page_size(),
                    end_cursor: cursor.to_string(),
                }))
                .send()
                .await?
                .json::<Response<repository_size::ResponseData>>()
                .await?;
            if let Some(errors) = response.errors {
                let messages: Vec<_> = errors.iter().map(|e| format!("{:}", e)).collect();
                warn!("GraphQL error (retry): {:?}", messages);
                return Err(Error::FailedGraphQL(messages));
            }
            Ok(response)
        };
        let response = closure.retry(&ExponentialBuilder::default()).await?;
        Ok(response)
    }
}
