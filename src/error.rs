use thiserror::Error as ThisError;

#[derive(ThisError, Debug)]
pub enum Error<'a> {
    #[error("request cannot be processed: `{0}`")]
    FailedRequest(#[from] reqwest::Error),

    #[error("failed GraphQL request: `{0:?}`")]
    FailedGraphQL(Vec<String>),

    #[error("invalid header: `{0}`")]
    InvalidHeader(#[from] reqwest::header::InvalidHeaderValue),

    #[error("conversion failed: `{0}`")]
    FailedConversion(#[from] std::num::TryFromIntError),

    #[error("no admin related queries can be performed")]
    InsufficientPermission,

    #[error("missing: `{0}`")]
    MissingData(&'a str),
}

pub type Result<T> = std::result::Result<T, Error<'static>>;
