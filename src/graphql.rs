use crate::error::Result;
use crate::repository_stats::RepositoryStatistics;
use compare::{natural, Compare};
use reqwest::Client;
use std::collections::HashMap;
use std::env;

const DEFAULT_PAGE_SIZE: i64 = 50;
const METRIC_PREFIX: &str = "gitlab_exporter_";

/// User agent that will be used for requests.
static APP_USER_AGENT: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

pub struct Metrics {
    metrics: HashMap<String, usize>,
    repository_statistics_vector: Vec<RepositoryStatistics>,
}

impl Metrics {
    pub fn new() -> Metrics {
        Metrics {
            metrics: HashMap::new(),
            repository_statistics_vector: Vec::new(),
        }
    }
    pub fn add_metric(&mut self, metric_name: String, metric_value: usize) {
        self.metrics
            .insert(format!("{METRIC_PREFIX}{metric_name}"), metric_value);
    }

    pub fn get_metrics(&self) -> HashMap<String, usize> {
        self.metrics.clone()
    }

    pub fn get_top_nth_filter_by(
        &self,
        nth: usize,
        map: &dyn Fn(&RepositoryStatistics) -> (String, i64),
    ) -> Vec<(String, i64)> {
        let mut sorted_list: Vec<(String, i64)> = self
            .repository_statistics_vector
            .iter()
            .map(map)
            .filter(|(_, size)| size > &(0_i64))
            .collect();
        sorted_list.sort_by(|(_, a_size), (_, b_size)| natural().compare(b_size, a_size));
        if sorted_list.len() > nth {
            let _ = sorted_list.drain(nth..);
        }
        sorted_list
    }

    pub fn set_repository_statistics_vector(
        &mut self,
        repository_statistics_vector: Vec<RepositoryStatistics>,
    ) {
        self.repository_statistics_vector = repository_statistics_vector;
    }
}

pub struct GraphQL {
    client: Client,
    page_size: i64,
    gitlab_url: String,
}

impl GraphQL {
    pub fn new(gitlab_url: String, gitlab_token: &str) -> Result<GraphQL> {
        let client = Client::builder()
            .user_agent(APP_USER_AGENT)
            .default_headers(
                std::iter::once((
                    reqwest::header::AUTHORIZATION,
                    reqwest::header::HeaderValue::from_str(&format!("Bearer {gitlab_token}"))?,
                ))
                .collect(),
            )
            .build()?;
        Ok(GraphQL {
            client,
            page_size: DEFAULT_PAGE_SIZE,
            gitlab_url,
        })
    }

    pub fn client(&self) -> &Client {
        &self.client
    }

    pub fn page_size(&self) -> i64 {
        self.page_size
    }

    pub fn gitlab_url(&self) -> &String {
        &self.gitlab_url
    }
}
