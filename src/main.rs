use clap::Parser;
use env_logger::Env;
use gitlab_exporter::args::Args;
use log::{debug, error};

#[tokio::main]
async fn main() {
    let args = Args::parse();
    let logging = match args.verbose {
        0 => "info",
        1 => "gitlab-exporter=debug",
        _ => "debug",
    };

    env_logger::init_from_env(Env::default().default_filter_or(logging));
    debug!("{:?}", args);

    if let Err(err) = gitlab_exporter::run(args).await {
        error!("Error: {:?}", err);
        for cause in err.chain() {
            error!("Caused by: {:?}", cause)
        }
        std::process::exit(1)
    }
    std::process::exit(0)
}
