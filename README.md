# gitlab-exporter

## Description

gitlab-exporter extracts metrics from GitLab using GraphQL API and outputs them in a prometheus compatible format

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).

## Usage

gitlab-exporter requires the following environment variables to be set:

* GITLAB_EXPORTER_GITLAB_API_URL - Gitlab GraphQL API url (defaults to `https://gitlab.archlinux.org/api/graphql`)
* GITLAB_EXPORTER_TOP_NTH - Number of top-of metrics to calculate (defaults to 10)
* GITLAB_EXPORTER_GITLAB_TOKEN - Create one from Gitlab UI
![Access Token](docs/admin_token_ro.png?raw=true "Access Token")

### Examples
#### usage
```shell

./target/release/gitlab-exporter 
gitlab-exporter 0.1.0
Leonidas Spyropoulos <artafinde@archlinux.org>, Orhun Parmaksız <orhun@archlinux.org>
Extracts metrics from GitLab's GraphQL API and produce prometheus consumable metrics

USAGE:
    gitlab-exporter [OPTIONS] --api-token <API_TOKEN> <SUBCOMMAND>

OPTIONS:
    -u, --api-url <API_URL>        GitLab GraphQL API URL [env: GITLAB_EXPORTER_GITLAB_API_URL=]
                                   [default: https://gitlab.archlinux.org/api/graphql]
    -t, --api-token <API_TOKEN>    GitLab API token [env: GITLAB_EXPORTER_GITLAB_TOKEN=]
    -n, --top-nth <TOP_NTH>        Number of top-of metrics to calculate [env:
                                   GITLAB_EXPORTER_TOP_NTH=] [default: 10]
    -p, --progress-bar             Show progress indicator
    -v, --verbose                  Verbose mode (-v, -vv, -vvv, etc.)
    -o <OUTPUT>                    Output file, stdout if not present
    -h, --help                     Print help information
    -V, --version                  Print version information

SUBCOMMANDS:
    cli     Execute once and output in cli
    help    Print this message or the help of the given subcommand(s)
```
#### cli (stdout)
```shell
export GITLAB_EXPORTER_GITLAB_API_URL="https://gitlab.archlinux.org/api/graphql" 
export GITLAB_EXPORTER_GITLAB_TOKEN="admin-read-api-token" 
./target/debug/gitlab-exporter cli
# HELP gitlab_exporter_top_build_artifacts_sizes gitlab exporter top build artifacts sizes (in bytes)
# TYPE gitlab_exporter_top_build_artifacts_sizes gauge
gitlab_exporter_top_build_artifacts_sizes{fullname="archlinux/arch-boxes"} 6233396438
gitlab_exporter_top_build_artifacts_sizes{fullname="archlinux/archiso"} 7495653667
gitlab_exporter_top_build_artifacts_sizes{fullname="archlinux/archlinux-bugs-snapshotter"} 1621669234
gitlab_exporter_top_build_artifacts_sizes{fullname="archlinux/gluebuddy"} 1850215989
gitlab_exporter_top_build_artifacts_sizes{fullname="archlinux/releng"} 11326985943
gitlab_exporter_top_build_artifacts_sizes{fullname="artafinde/gluebuddy"} 553020097
gitlab_exporter_top_build_artifacts_sizes{fullname="michaelgilch/archiso"} 355842769
gitlab_exporter_top_build_artifacts_sizes{fullname="nl6720/arch-boxes"} 2012390432
gitlab_exporter_top_build_artifacts_sizes{fullname="tallero/archiso"} 10836823096
gitlab_exporter_top_build_artifacts_sizes{fullname="tallero/archiso-profiles"} 562933431
# HELP gitlab_exporter_top_commit_counts gitlab exporter top commit counts
# TYPE gitlab_exporter_top_commit_counts gauge
gitlab_exporter_top_commit_counts{fullname="anthraxx/pacman"} 7144
gitlab_exporter_top_commit_counts{fullname="diabonas/pacman"} 7144
gitlab_exporter_top_commit_counts{fullname="dvzrv/linux-rt"} 1105012
gitlab_exporter_top_commit_counts{fullname="dvzrv/linux-rt-lts"} 275149
gitlab_exporter_top_commit_counts{fullname="foxboron/pacman"} 7144
gitlab_exporter_top_commit_counts{fullname="jelle/pacman"} 7144
gitlab_exporter_top_commit_counts{fullname="mmdbalkhi/pacman"} 7144
gitlab_exporter_top_commit_counts{fullname="pacman/pacman"} 7144
gitlab_exporter_top_commit_counts{fullname="pedanticdm/pacman"} 7144
gitlab_exporter_top_commit_counts{fullname="soloturn/pacman"} 7144
# HELP gitlab_exporter_top_lfs_objects_sizes gitlab exporter top lfs objects sizes (in bytes)
# TYPE gitlab_exporter_top_lfs_objects_sizes gauge
gitlab_exporter_top_lfs_objects_sizes{fullname="archlinux/conf-files"} 27507213284
# HELP gitlab_exporter_top_packages_sizes gitlab exporter top package sizes (in bytes)
# TYPE gitlab_exporter_top_packages_sizes gauge
gitlab_exporter_top_packages_sizes{fullname="archlinux/arch-boxes"} 6048056763
gitlab_exporter_top_packages_sizes{fullname="archlinux/archlinux-docker"} 20215884200
# HELP gitlab_exporter_top_pipeline_artifacts_sizes gitlab exporter top pipeline artifacts sizes (in bytes)
# TYPE gitlab_exporter_top_pipeline_artifacts_sizes gauge
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="archlinux/archlinux-keyring"} 101136
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="archlinux/aurweb"} 19546023
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="archlinux/repod"} 646769
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="artafinde/aurweb"} 1549718
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="fluix/aurweb"} 2297939
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="fosskers/aurweb"} 613394
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="hwittenborn/aurweb"} 1414531
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="jelle/aurweb"} 170163
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="jocke-l/aurweb"} 228909
gitlab_exporter_top_pipeline_artifacts_sizes{fullname="kevr/aurweb"} 48664653
# HELP gitlab_exporter_top_repository_sizes gitlab exporter top repository sizes (in bytes
# TYPE gitlab_exporter_top_repository_sizes gauge
gitlab_exporter_top_repository_sizes{fullname="archlinux/archlinux-bugs-snapshotter"} 909964738
gitlab_exporter_top_repository_sizes{fullname="archlinux/archlinux-docker"} 82239815
gitlab_exporter_top_repository_sizes{fullname="archlinux/conf"} 19943915
gitlab_exporter_top_repository_sizes{fullname="archlinux/conf-files"} 114043125
gitlab_exporter_top_repository_sizes{fullname="dvzrv/linux-rt"} 2588797829
gitlab_exporter_top_repository_sizes{fullname="dvzrv/linux-rt-lts"} 2934607708
gitlab_exporter_top_repository_sizes{fullname="jelle/archweb"} 15246295
gitlab_exporter_top_repository_sizes{fullname="mackilanu/aurweb"} 15676211
gitlab_exporter_top_repository_sizes{fullname="pacman/alpm.rs"} 12341739
gitlab_exporter_top_repository_sizes{fullname="victor3d/test"} 171672862
# HELP gitlab_exporter_top_snippets_sizes gitlab exporter top snippets sizes sizes (in bytes)
# TYPE gitlab_exporter_top_snippets_sizes gauge
gitlab_exporter_top_snippets_sizes{fullname="archlinux/infrastructure"} 115343
gitlab_exporter_top_snippets_sizes{fullname="archlinux/service-desks/account-support"} 104857
# HELP gitlab_exporter_top_storage_sizes gitlab exporter top storage sizes (in bytes)
# TYPE gitlab_exporter_top_storage_sizes gauge
gitlab_exporter_top_storage_sizes{fullname="archlinux/arch-boxes"} 12282285534
gitlab_exporter_top_storage_sizes{fullname="archlinux/archiso"} 7502111087
gitlab_exporter_top_storage_sizes{fullname="archlinux/archlinux-bugs-snapshotter"} 2531633972
gitlab_exporter_top_storage_sizes{fullname="archlinux/archlinux-docker"} 20530118128
gitlab_exporter_top_storage_sizes{fullname="archlinux/conf-files"} 27621256409
gitlab_exporter_top_storage_sizes{fullname="archlinux/releng"} 11327227115
gitlab_exporter_top_storage_sizes{fullname="dvzrv/linux-rt"} 2588797829
gitlab_exporter_top_storage_sizes{fullname="dvzrv/linux-rt-lts"} 2934607708
gitlab_exporter_top_storage_sizes{fullname="nl6720/arch-boxes"} 2012589660
gitlab_exporter_top_storage_sizes{fullname="tallero/archiso"} 10837431269
# HELP gitlab_exporter_top_uploads_sizes gitlab exporter top uploads sizes sizes (in bytes)
# TYPE gitlab_exporter_top_uploads_sizes gauge
gitlab_exporter_top_uploads_sizes{fullname="archlinux/archiso"} 1455713
gitlab_exporter_top_uploads_sizes{fullname="archlinux/archlinux-common-style"} 840137
gitlab_exporter_top_uploads_sizes{fullname="archlinux/archlinux-keyring"} 1616672
gitlab_exporter_top_uploads_sizes{fullname="archlinux/aurweb"} 10062114
gitlab_exporter_top_uploads_sizes{fullname="archlinux/gluebuddy"} 87717188
gitlab_exporter_top_uploads_sizes{fullname="archlinux/infrastructure"} 298893819
gitlab_exporter_top_uploads_sizes{fullname="archlinux/legal/privacy-requests"} 2364570
gitlab_exporter_top_uploads_sizes{fullname="archlinux/legal/trademark-requests"} 1639771
gitlab_exporter_top_uploads_sizes{fullname="archlinux/rfcs"} 269624
gitlab_exporter_top_uploads_sizes{fullname="archlinux/service-desks/forum"} 1319836
# HELP gitlab_exporter_top_wiki_sizes gitlab exporter top wiki sizes sizes (in bytes)
# TYPE gitlab_exporter_top_wiki_sizes gauge
gitlab_exporter_top_wiki_sizes{fullname="Kppqju77/arch-boxes"} 83886
gitlab_exporter_top_wiki_sizes{fullname="archlinux/archlinux-keyring"} 304087
gitlab_exporter_top_wiki_sizes{fullname="archlinux/aurweb"} 199229
gitlab_exporter_top_wiki_sizes{fullname="archlinux/infrastructure"} 199229
gitlab_exporter_top_wiki_sizes{fullname="archlinux/legal/privacy-requests"} 356515
gitlab_exporter_top_wiki_sizes{fullname="kevr/aurweb"} 136314
gitlab_exporter_top_wiki_sizes{fullname="kevr/pacman"} 73400
gitlab_exporter_top_wiki_sizes{fullname="kevr/pyalpm"} 73400
gitlab_exporter_top_wiki_sizes{fullname="raulrosilalco11/fosscord-client"} 73400
gitlab_exporter_top_wiki_sizes{fullname="tpowa/archboot"} 262144
# HELP gitlab_exporter_total_build_artifacts_size gitlab exporter total build artifacts size (in bytes)
# TYPE gitlab_exporter_total_build_artifacts_size gauge
gitlab_exporter_total_build_artifacts_size 45221150003
# HELP gitlab_exporter_total_commit_count gitlab exporter total commit count
# TYPE gitlab_exporter_total_commit_count gauge
gitlab_exporter_total_commit_count 2146321
# HELP gitlab_exporter_total_lfs_objects_size gitlab exporter total lfs objects size (in bytes)
# TYPE gitlab_exporter_total_lfs_objects_size gauge
gitlab_exporter_total_lfs_objects_size 27507213284
# HELP gitlab_exporter_total_packages_size gitlab exporter total packages size (in bytes)
# TYPE gitlab_exporter_total_packages_size gauge
gitlab_exporter_total_packages_size 26263940963
# HELP gitlab_exporter_total_pipeline_artifacts_size gitlab exporter total pipeline artifacts size (in bytes)
# TYPE gitlab_exporter_total_pipeline_artifacts_size gauge
gitlab_exporter_total_pipeline_artifacts_size 75917966
# HELP gitlab_exporter_total_repository_size gitlab exporter total repository size (in bytes)
# TYPE gitlab_exporter_total_repository_size gauge
gitlab_exporter_total_repository_size 11161604484
# HELP gitlab_exporter_total_snippets_size gitlab exporter total snippets size (in bytes)
# TYPE gitlab_exporter_total_snippets_size gauge
gitlab_exporter_total_snippets_size 220200
# HELP gitlab_exporter_total_storage_size gitlab exporter total storage size (in bytes)
# TYPE gitlab_exporter_total_storage_size gauge
gitlab_exporter_total_storage_size 110647279708
# HELP gitlab_exporter_total_uploads_size gitlab exporter total uploads size (in bytes)
# TYPE gitlab_exporter_total_uploads_size gauge
gitlab_exporter_total_uploads_size 407323870
# HELP gitlab_exporter_total_wiki_size gitlab exporter total wiki size (in bytes)
# TYPE gitlab_exporter_total_wiki_size gauge
gitlab_exporter_total_wiki_size 9908938
# HELP gitlab_exporter_trends_total_groups gitlab exporter trends total groups
# TYPE gitlab_exporter_trends_total_groups gauge
gitlab_exporter_trends_total_groups 25
# HELP gitlab_exporter_trends_total_projects gitlab exporter trends total projects
# TYPE gitlab_exporter_trends_total_projects gauge
gitlab_exporter_trends_total_projects 9591
# HELP gitlab_exporter_trends_total_users gitlab exporter trends total users
# TYPE gitlab_exporter_trends_total_users gauge
gitlab_exporter_trends_total_users 1189
```

## Release How-To
1. Adjust release in `Cargo.toml`
2. Commit and tag release in git
   ```shell
   git commit --gpg-sign --message "chore(release): version v${VERSION}" Cargo.toml
   git tag --sign --message "Version v${VERSION}" v${VERSION}
   ```
3. Create and sign source archive artifact
    ```shell
    git archive --format=tar --prefix=gitlab-exporter-${VERSION}/ v${VERSION} | gzip > gitlab-exporter-${VERSION}.tar.gz
	gpg --detach-sign --use-agent gitlab-exporter-${VERSION}.tar.gz
    ```
4. Create Gitlab Release and upload artifacts
    ```shell
    glab release create v$(VERSION) gitlab-exporter-$(VERSION).tar.gz*
    ```