# TODOs

# Features

- [x] Monitor GitLab Artifact Size
- [x] Monitor Gitlab Number of Projects
- [x] Monitor Gitlab Number of Namespaces
- [x] Monitor Gitlab Project Repository Size, Docker size, Artefact Size (node-exporter to monitor Gitlab directories?)
- [x] Monitor Gitlab Number of Users
- [x] Export to a format compatible with prometheus

# Tech debt

- [x] Deduplicate code for graphql queries
- [ ] Expose metrics with http
- [x] Add gitlab url and token as args
- [x] Switch to graphql using `usageTrendsMeasurements` (requires `GITLAB admin token`)

```graphql
query Users {
  usageTrendsMeasurements(identifier: USERS) {
    edges {
      node {
        count
      }
    }
  }
}
```
