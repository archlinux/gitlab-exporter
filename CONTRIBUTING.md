# Contributing to gitlab-exporter

## Formatting

Please format the code using `cargo fmt`

## Building

gitlab-exporter is built with `cargo` and `Makefile`

## Testing
You can run a local gitlab instance with docker, check [docs](https://docs.gitlab.com/ee/install/docker.html)

## Exporting GitLab GraphQL API schema

Gitlab might update the GraphQL API schema so in newer versions you might need to export a new schema and save it See [schema.json](../graphql/schema.json). The easiest way is to use [graphql-client](https://github.com/graphql-rust/graphql-client/tree/main/graphql_client_cli) and introspect-schema the schema
```
graphql-client introspect-schema <schema_location_url> > schema.json
```
